console.log("Hello World!");

let numA = 10;
let numB = 15;

let sum = numA + numB;
let difference = numA - numB;
let product = numA * numB;
let quotient = numA / numB;

console.log("The sum of the two numbers is: " + sum);
console.log("The difference of the two numbers is: " + difference);
console.log("The product of the two numbers is: " + product);
console.log("The quotient of the two numbers is: " + quotient);


console.log("The sum is greater than the difference: "+(sum > difference));
console.log("The product and quotient are positive numbers: " + (product > 0 && quotient > 0));
console.log("One of the results is negative: "+(product < 0 || difference < 0 || quotient < 0 || product < 0));
console.log("All the results are not equal to zero: " +(product != 0 && difference != 0 && quotient != 0 && product != 0) )
